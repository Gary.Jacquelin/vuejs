import { defineStore } from 'pinia'
import { ref, watch } from 'vue'

export const useCartStore = defineStore('cart', () => {
  const cart = ref([])

  const isCart = (product) => cart.value.some((s) => s.id === product.id)

  const addToCart = (product) => {
    !product.qty ? (product.qty = 1) : product.qty++
    if (!isCart(product)) {
      cart.value.push(product)
    }
  }

  const removeFromCart = (product) => {
    cart.value = cart.value.filter((s) => s.id !== product.id)
  }

  const removeAllFromCart = () => {
    cart.value = []
  }

  // persist
  const cartLocal = localStorage.getItem('cartProducts')
  if (cartLocal) {
    cart.value = JSON.parse(cartLocal)
  }

  watch(
    cart,
    () => {
      localStorage.setItem('cartProducts', JSON.stringify(cart.value))
    },
    { deep: true }
  )

  return { cart, isCart, addToCart, removeFromCart, removeAllFromCart }
})
