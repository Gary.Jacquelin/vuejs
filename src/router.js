import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import CategoriesView from '@/views/CategoriesView.vue'
import CategoryView from '@/views/CategoryView.vue'
import CartView from '@/views/CartView.vue'
import ProductView from '@/views/ProductView.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/categories',
      name: 'categories',
      component: CategoriesView,
      children: [
        {
          path: ':tag',
          name: 'category',
          component: CategoryView,
          props: (route) => ({
            categoryName: route.params.tag
          })
        }
      ]
    },
    {
      path: '/product/:id',
      name: 'product',
      component: ProductView,
      props: (route) => ({
        id: route.params.id
      })
    },
    {
      path: '/cart',
      name: 'cart',
      component: CartView
    }
  ],
  linkActiveClass: 'is-active'
})

export default router
