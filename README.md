Membre du groupe :

- Théo Tanchoux
- Gary Jacquelin

Description projet :
Site de E-commerce où l'on peut ajouter des produits au panier, les supprimer, modifier les quantités et voir le detail de
chaque produit.

Hebergement :
https://dreamy-smakager-120780.netlify.app/
